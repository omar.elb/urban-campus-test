package main

import (
	// "fmt"
	"log"
	"net"
	"context"
	"errors"
	"time"
	"strconv"
	"encoding/json"
	"html"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	news "hackernews/test/github.com/oz/hn"
	hnOrigin "github.com/peterhellberg/hn"
	redis "github.com/go-redis/redis/v8"
)

type server struct {
	news.UnimplementedHnServiceServer
}

var cache = redis.NewClient(&redis.Options{
	Addr: "localhost:6379",
})

var ctx = context.Background()

func checkTopStoriesCache() ([]int, error) {
	storedIds, err := cache.Get(ctx, "news_ids").Bytes()
	if err != nil {
		return []int{}, errors.New("No news ids found in cache, or ttl elapsed")
	}
	var ids []int 
	e := json.Unmarshal(storedIds, &ids)
	return ids, e
}

func updateTopStoriesCache(ids []int) {
	toCacheIds, e := json.Marshal(ids)
	if e != nil {
		log.Fatalf("could not pickle ids")
	}
	cache.Set(ctx, "news_ids", toCacheIds, 3600*time.Second)
}

func getTopStoriesIds() []int {
	hnOrigin := hnOrigin.DefaultClient
	cachedIds, err := checkTopStoriesCache()
	var ids []int
	if err == nil {
		ids = cachedIds
	} else {
		liveIds, err := hnOrigin.TopStories()
		ids = liveIds
		if err != nil {
			log.Fatalf("Failed to retrieve top stories: %v", err)
		}
		updateTopStoriesCache(ids)
	}
	return ids
}

func checkItemCache(id int) (*news.Story, error) {
	itemCached, err := cache.Get(ctx, strconv.Itoa(id)).Bytes()
	var item *news.Story
	if err != nil {
		return item, errors.New("No Item found in cache, or ttl elapsed")
	}
	e := json.Unmarshal(itemCached, &item)
	return item, e
}

func updateItemCache(id int, item *news.Story) {
	pickledItem, e := json.Marshal(item)
	if e != nil {
		log.Fatalf("Could not pickle item")
	}
	cache.Set(ctx, strconv.Itoa(id), pickledItem, 1200*time.Second)
}

func getStoryItem(id int) *news.Story {
	hnOrigin := hnOrigin.DefaultClient
	cachedItem, err := checkItemCache(id)
	var item *news.Story
	if err == nil {
		item = cachedItem
	} else {
		itemOrigin, e := hnOrigin.Item(id)
		item = new(news.Story)
		item.Title = itemOrigin.Title
		item.Url = itemOrigin.URL
		if e != nil {
			log.Fatalf("Failed to retrieve top story item: %v", e)
		}
		updateItemCache(id, item)
	}
	return item
}

func (s* server) GetTopStories(in *news.TopStoriesRequest, stream news.HnService_GetTopStoriesServer) error {
	ids := getTopStoriesIds()
	for _, id := range ids[:25] {
		item := getStoryItem(id)
		if len(item.Title) > 0 {
			if erreur := stream.Send(item); erreur != nil {
				return erreur
			}
		}
	}
	return nil
}

func checkUserCache(nick string) (*news.User, error) {
	userCached, err := cache.Get(ctx, nick).Bytes()
	var user *news.User
	if err != nil {
		return user, errors.New("No user found in cache, or ttl elapsed")
	}
	e := json.Unmarshal(userCached, &user)
	return user, e
}

func updateUserCache(nick string, user *news.User) {
	pickledUser, e := json.Marshal(user)
	if e != nil {
		log.Fatalf("Could not pickle user")
	}
	cache.Set(ctx, nick, pickledUser, 86400*time.Second)
}

func (s* server) Whois(ctx context.Context, in *news.WhoisRequest) (*news.User, error) {
	hnOrigin := hnOrigin.DefaultClient
	cachedUser, err := checkUserCache(in.Nick)
	var user *news.User
	if err == nil {
		user = cachedUser
	} else {
		userOrigin, e := hnOrigin.User(in.Nick)
		if userOrigin.Karma == 0 {
			return user, errors.New("No user with this nichname")
		}
		user = new(news.User)
		user.Karma = uint64(userOrigin.Karma)
		user.About = html.UnescapeString(userOrigin.About)
		user.JoinedAt = time.Unix(int64(userOrigin.Created), 0).Format("2006-01-02 15:04:05")
		if e != nil {
			log.Fatalf("Failed to retrieve top story user: %v", e)
		}
		updateUserCache(in.Nick, user)
	}
	return user, nil
}

func getRealStories() []*news.Story {
	hnOrigin := hnOrigin.DefaultClient
	ids, err := hnOrigin.TopStories()
	if err != nil {
		log.Fatalf("Failed to retrieve top stories: %v", err)
	}

	stories := []*news.Story {}
	for id := range ids {
		item, e := hnOrigin.Item(id)
		if e != nil {
			log.Fatalf("Failed to retrieve top story item: %v", e)
		}
		if len(item.Title) > 0 {
			var story *news.Story
			story = new(news.Story)
			story.Title = item.Title
			story.Url = item.URL
			stories = append(stories, story)
		}
	}
	return stories
}

func main() {
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	log.Println("Listening")

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)
	news.RegisterHnServiceServer(grpcServer, &server{})
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
}
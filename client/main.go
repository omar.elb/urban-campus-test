package main

import (
	"fmt"
	"log"
	"io"
	"context"
	"flag"
	"strconv"

	"google.golang.org/grpc"
	news "hackernews/test/github.com/oz/hn"
)

func main() {
	whois := flag.String("whois", "", "Pseudo Name of user you want to look up")
	list := flag.Bool("list", false, "List latest Hacker News")
	flag.Parse()
	if (*whois == "" && !*list) || (flag.NFlag() > 1) {
		log.Fatalf("You must choose an action by setting ONE of the flags [-whois <name>, -list]")
	}

	conn, err := grpc.Dial("localhost:9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to dial backend server %v", err)
	}
	client := news.NewHnServiceClient(conn)

	if *list {
		stream, err := client.GetTopStories(context.Background(), &news.TopStoriesRequest{})
		if err != nil {
			log.Fatalf("Failed to get stream of top stories %v", err)
		}
		for {
			data, e := stream.Recv()
			if e == io.EOF {
				return
			} else if e != nil {
				log.Fatalf("Cannot stream data correctly %v", e)
			}
			listItem := fmt.Sprintf("- %s\n%s\n", data.Title, data.Url)
			fmt.Println(listItem)
		}
	} else if *whois != "" {
		user, err := client.Whois(context.Background(), &news.WhoisRequest{Nick:*whois})
		if err != nil {
			log.Fatalf("Failed to get user data %v", err)
		}
		fmt.Println(fmt.Sprintf("User:\t%s\nKarma:\t%s\nAbout:\t%s\nJoined:\t%s", *whois, strconv.Itoa(int(user.Karma)), user.About, user.JoinedAt))
	}

}

# Hacker News Project
This project is an implementation for a server-client application that displays information about users and latest news. It serves as a demo for Urban Campus rectument process.

## Installation and setup
### Go packages
Make sure to have a stable version of Go installed
some packages need to be installed:
```
go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest
go get github.com/peterhellberg/hn
```

When first starting the project, you may encouter problem of unfound packages. In that case, the mod file has to be initiated:
```
export GO111MODULE=on
go mod init hackernews/test
```

Compiling proto file:
```
protoc --proto_path=proto proto/*.proto --go_out=. --go-grpc_out=.
```

Testing the server with grpcul:
```
grpcurl -plaintext localhost:9000 list
grpcurl -plaintext localhost:9000 hn.HnService/GetTopStories
```

### Redis installation
There's a need to have redis up and running on the same server as the Go server:
Here's the official documentation to get it done: [Redis Install Instructions](https://redis.io/docs/getting-started/installation/install-redis-on-linux/)

Make sure that redis is running on port 6379 (Default port)

## Running and testing
If everything is set up correctly, you should be able to test the server by:
* First running the server:
```
go run server/main.go
```

* Running the client to test the functionality:
```
run go client/main.go -list
```
```
run go client/main.go -whois es
```

## Notes
Some assumptions were made in this project that you should be aware of:
* For the listing functionality ```client -list```. A limit of 25 is applied by default. It can be changed from the server. If you want to change it, you should also devalidate the cache.
* A specific Time To Live has been defined for each type of object cached:
    * The Posts' ids from Hacker News API TTL: 1hour
    * The Post Item TTL: 20min
    * The user data TTL: 24hours